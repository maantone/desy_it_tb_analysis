# import csv mask fle Ph2ACF for croc and create mask txt for corry
# 50x50 as starting point mapping
import csv
import math

csv_file="/home/cmspix/daq/desy_it_tb_analysis/Mask_files/enable_1_09062022.csv"
Output_File = open("Mask_files/mask_703.txt","w")
type = "25x100origR0C0"
cols = 432; rows = 336; r=0

def Converter25x100origR0C0(row, col):
    row    = 2 * row + (col % 2)
    col    = math.floor(col/2)
    return row, col

with open(csv_file) as f:
    csvreader = csv.reader(f)
    for row in csvreader:
        c=0
        for col in row:
            if col=='0': 
                if type == "25x100origR0C0": 
                    rc,cc=Converter25x100origR0C0(r,c)
                    Output_File.write('p\t'+str(cc)+'\t'+str(rc)+'\n')
                else: Output_File.write('p\t'+str(c)+'\t'+str(r)+'\n')
            c=c+1
        r=r+1
