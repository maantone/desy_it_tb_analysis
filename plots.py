import matplotlib.pyplot as plt
plt.rc('axes', labelsize=16, titlesize=16)
plt.rc('ytick', labelsize=16)
plt.rc('xtick', labelsize=16)
plt.rc('text', usetex=True)

X=()
Y=()  

#my_dpi=96
fig = plt.figure()#figsize=(1490/my_dpi, 750/my_dpi), dpi=my_dpi)#figsize=(10, 8)
ax = fig.add_subplot(111)
ax.plot(X, Y, label='FTH150P RD53A 25x100 P1 (Bitten) ',marker='.',markersize=12, linestyle=':')
ax.grid(b=True, which='major', color='#999999', linestyle=':', alpha=1.0)
ax.minorticks_on()
ax.grid(b=True, which='minor', color='#999999', linestyle=':', alpha=0.4)
ax.set(title = 'Cluster Size Col', xlabel = 'Angle [deg]', ylabel = 'Cluster Size [col]')
ax.set_xlim(70, 86)
ax.set_ylim(0, 25)
fig.tight_layout()
ax.legend(prop={'size': 12})
plt.show()
    
#fig.savefig(PathNameSave+'_IV.png', format='png', dpi=300)